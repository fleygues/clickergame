var nbClics = 0, // On déclare la variable "score" qui s'incrémente à chaque clic du joueur dans la zone de jeu
    record = localStorage.getItem("meilleurScore"), // On déclare la variable "record" qui récupère le meilleur score enregistré dans le localStorage. ATTENTION : s'il n'y a pas de clé "meilleurScore" dans le localStorage, la valeur de la variable "record" sera "null". Dans ce cas, il est pertinent de vérifier si la clé existe dans le localStorage, exemple avec un ternaire qui récupère la valeur si la clé existe et sinon affecte la valeur 0 : record = "meilleurScore" in localStorage ? localStorage.getItem("meilleurScore") : 0
    count, // On déclare la variable "count" qui sera utilisée pour afficher un décompte du temps restant au joueur
    myTimer, // On déclare la variable "myTimer" qui va nous permettre de gérer le décompte du temps restant au joueur
    time, // On déclare la variable "time" qui va contenir la durée du jeu (en ms)
    pseudo; // On déclare la variable "pseudo" qui va contenir le pseudo du joueur

console.log(`Le record actuel est de ${record}`); // On affiche dans la console le record actuellement enregistré dans le localStorage

// On déclare un écouteur d'événement lors de la validation (submit) du formulaire
$("#introForm").on("submit", function(event) {
    event.preventDefault(); // On empêche le formulaire de recharger la page

    pseudo = $("#pseudo").val(); // On affecte le pseudo du joueur rentré dans la balise input (id #pseudo) à la variable "pseudo"
    time = $("#time").val(); // On affecte le temps du jeu rentré dans la balise input (id #time) à la variable "time"

    console.log(`Le temps de jeu est de ${time}ms et le joueur est ${pseudo}`);

    $("#intro").fadeOut(function(){ // On fait disparaître la balise div contenant le formulaire
       $("#game").fadeIn(); // ... et quand l'animation est complète, on affiche la balise div contenant le jeu
    });    
});

// On déclare un écouteur d'événement sur la zone de jeu (id #gameArea dans la page html) du clic (à l'aide de la méthode .on() de jQuery) qui appelle la fonction clickAndDisplay() à chaque clic
$("#gameArea").on("click", clickAndDisplay);

// On déclare un écouteur d'événement sur le bouton recommencer (id #btnRetry dans la page html) du clic (à l'aide de la méthode .on() de jQuery) qui appelle la fonction anonyme fléchée 
$("#btnRetry").on("click", () => {
    console.log("On a cliqué sur le bouton recommencer"); // On affiche un message dans la console

    nbClics = 0; // On réinitialise la variable "score" à 0
    $('h2 span.rouge').text(nbClics); // ... et on l'affiche dans la page html à l'aide du sélecteur jQuery $() et de la méthode .text() de jQuery

    $("#btnRetry").fadeOut(); // On masque le bouton recommencer à l'aide de la méthode .fadeOut() de jQuery

    $("#gameArea").on("click", clickAndDisplay); // On met de nouveau un écouteur d'événement sur la zone de jeu
});

// -------------------------------- FUNCTIONS --------------------------------

// Déclaration de la fonction clickAndDisplay() qui est appelée à chaque fois que le joueur clique dans la zone du jeu
function clickAndDisplay() {
    nbClics++; // On incrémente la variable qui compte le nombre de clics
    $('h2 span.rouge').text(nbClics); // ... et on l'affiche dans la page html à l'aide du sélecteur jQuery $() et de la méthode .text() de jQuery

    if(nbClics == 1) { // Si le nombre de clic est 1, c-a-d si le joueur clique pour la première fois dans la zone du jeu
        setTimeout(gameOver, time); // ... On lance un décompte (setTimeout) qui appelera la fonction gameOver() après 3000ms (cette durée est définie dans la variable constante "time" déclarée au début de notre code)
        displayCount(); // ... On appelle la fonction displayCount() qui affiche le temps restant du joueur dans la console
    }
}

// Déclaration de la fonction gameOver() qui est appelée à chaque fois que le temps est écoulé
function gameOver() {
    alert("FIN DU GAME"); // On affiche un message à l'utilisateur
    $("#gameArea").off("click"); // On retire l'écouteur d'événement du clic sur la zone de jeu
    $("#btnRetry").fadeIn(); // On affiche le bouton pour recommencer à l'aide de la méthode .fadeIn() de jQuery

    clearInterval(myTimer); // On arrête le décompte de notre jeu en appelant la méthode clearInterval() de Javascript

    if(nbClics > record) { // Si le nombre de clics (le score) du joueur est supérieur au record connu
        record = nbClics; // ... On modifie la valeur de la variable "record" par le score du joueur
        console.log(`nouveau record : ${record}`); // ... On affiche le nouveau record dans la console

        localStorage.setItem("meilleurScore", record); // On enregistre dans le localStorage la valeur de la variable "record" avec la clé "meilleurScore"
    }
}

// Déclaration de la fonction displayCount() qui est appelée à chaque fois que le joueur clique pour la première fois dans la zone de jeu
function displayCount() {
    count = time/1000; // On définit la valeur de la variable "count" en fonction de la valeur de la variable time/1000 afin d'avoir une valeur en seconde et non en ms (time étant déclarée en ms)
    console.log(count); // On affiche le temps restant dans la console

    myTimer = setInterval(() => { // On lance un décompte à l'aide de la méthode Javascript setInterval() qui éxecute à chaque seconde le code suivant :
        count--; // ... On décrémente la variable "count"
        console.log(count); // ... et on l'affiche dans la console
    }, 1000); // 1000ms = 1s afin de créer un décompte qui affiche les secondes restantes
}